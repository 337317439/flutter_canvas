import 'dart:ui' as ui;
import 'package:flutter/material.dart';
import 'package:flutter_canvas/widget/comm.dart';
import 'package:flutter_canvas/widget/utils.dart';

class Anim58Page extends StatefulWidget {
  final String title;

  Anim58Page({Key? key, required this.title}) : super(key: key);

  @override
  _Anim58PageState createState() => _Anim58PageState();
}

class _Anim58PageState extends State<Anim58Page>
    with SingleTickerProviderStateMixin {
  late AnimationController _controller;
  // Offset _offset = Offset.zero;

  @override
  void initState() {
    _controller =
        AnimationController(duration: Duration(seconds: 1), vsync: this)
          ..repeat();
    _controller.addListener(() {
      if (mounted) {}
    });

    super.initState();
  }

  @override
  void dispose() {
    _controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: appBar(widget.title),
      body: AnimatedBuilder(
        animation: _controller,
        builder: (context, child) {
          return CustomPaint(
            size: Size(double.infinity, double.infinity),
            painter: MyCustomPainter(
              context: context,
            ),
          );
        },
      ),
      floatingActionButton: actionButton(() {
        setState(() {});
      }),
    );
  }
}

class MyCustomPainter extends CustomPainter {
  final BuildContext context;
  MyCustomPainter({required this.context});

  // Paint _paint = Paint()
  //   ..strokeCap = StrokeCap.round
  //   ..color = Colors.blue
  //   ..isAntiAlias = true
  //   ..strokeWidth = 1
  //   ..style = PaintingStyle.fill;

  @override
  void paint(Canvas canvas, Size size) {
    canvas.save();
    drawAuthorText(canvas);

    _drawText(canvas, size, 200);
    canvas.restore();

    canvas.save();
    // 文字粒子变化效果
    // 查找像素
    // 目前没有直接读取canvas画布像素方式，转成img效率不行
    for (var j = 0; j < size.height; j += 4) {
      for (var i = 0; i < size.width; i += 4) {
        var opacityIndex = (i + j * size.width) * 4 + 3;
        print(opacityIndex);
        // if (imageData[opacityIndex] > 0) {
        //   // 放入粒子对象
        //   var ball = Ball()
        //   list.add(par)
        // }
      }
    }

    canvas.restore();
  }

  void _drawText(Canvas canvas, Size size, double fontSize) {
    ui.ParagraphBuilder paragraphBuilder = ui.ParagraphBuilder(
      ui.ParagraphStyle(),
    );
    paragraphBuilder.pushStyle(ui.TextStyle(
      color: Colors.red,
      fontSize: fontSize,
      fontWeight: FontWeight.bold,
    ));
    var str = "文字";
    paragraphBuilder.addText(str);
    ui.ParagraphConstraints paragraphConstraints =
        ui.ParagraphConstraints(width: size.width);
    ui.Paragraph paragraph = paragraphBuilder.build()
      ..layout(paragraphConstraints);
    Size textSize = calculateTextSize(
      context,
      str,
      TextStyle(fontWeight: FontWeight.bold, fontSize: fontSize),
    );
    Offset center = size.center(Offset.zero);
    canvas.drawParagraph(
        paragraph,
        Offset(
            center.dx - textSize.width / 2, center.dy - textSize.height / 2));
  }

  @override
  bool shouldRepaint(CustomPainter oldDelegate) {
    return true;
  }
}
